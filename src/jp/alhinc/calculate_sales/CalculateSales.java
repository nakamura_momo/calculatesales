package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String FILE_OVER_DIGIT = "合計金額が10桁を超えました";
	private static final String FILE_BRANCH_CODE_EXIST = "の支店コードが不正です";
	private static final String FILE_COMMODITY_CODE_EXIST = "の商品コードが不正です";
	private static final String FILE_LINES_ERROR = "のフォーマットが不正です";
	

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が渡されているか判定
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,"^\\d{3}$", "支店")){
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,"^[A-Za-z0-9]{8}$", "商品")) {
			return;
		}

		//filesに全てのファイル情報を格納
		File[] files = new File(args[0]).listFiles();
		//リストの作成
		List<File> rcdFiles = new ArrayList<>();
		
		for(int i = 0; i < files.length ; i++) {
			//ファイル名の取得
			files[i].getName();
			//ファイルかつ拡張子がrcdでファイル名が数字8桁
			if(files[i].isFile() && files[i].getName().matches("^\\d{8}+.rcd")){
				//売上ファイルパスのみリストに格納
				rcdFiles.add(files[i]);
			}	
		}
		//リストを昇順にソート
		Collections.sort(rcdFiles);
		//連番判定：比較回数は売上ファイルの数よりも1回少なくなるため上限はファイル数-1
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			//連番でない場合
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}
		
		BufferedReader br = null;
		String line;

		//売上ファイルがなくなるまで繰り返し
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				//リストを作成
				List<String> data = new ArrayList<>();
				//該当ファイルの読み込み
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				//dataリストに支店コード、商品コード、売上金額を入れる
				while((line = br.readLine()) != null) {
					data.add(line);
				}
				// 売上ファイルの中身が3行でない
				if(data.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_LINES_ERROR);
					return;
				}
				//支店コードが該当なし
				if (!branchSales.containsKey(data.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_BRANCH_CODE_EXIST);
					return;
				}
				//商品コードが該当なし
				if (!commoditySales.containsKey(data.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_COMMODITY_CODE_EXIST);
					return;
				}
				//売上金額が数字か判定
				if(!data.get(2).matches("^\\d*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//String型をlong型に変換
				long fileSale = Long.parseLong(data.get(2));
				//合計金額の加算
				Long saleAmount = branchSales.get(data.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(data.get(1)) + fileSale;
				//売上金額が10桁超え：末尾L=long型
				if(saleAmount >= 10000000000L || commodityAmount >= 10000000000L){
					System.out.println(FILE_OVER_DIGIT);
					return;
				}
				//合計金額を支店別マップに保持
				branchSales.put(data.get(0), saleAmount);
				//合計金額を商品別マップに保持
				commoditySales.put(data.get(1), commodityAmount);
				
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		
		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName,  Map<String, String> names, Map<String, Long> sales, String regular, String fileType) {		
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//支店定義ファイルもしくは商品定義ファイルが存在しない
			if(!file.exists()) {
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			
			 // 一行ずつ読み込みデータがなくなるまで繰り返す
			while((line = br.readLine()) != null) { 
				String[] read = line.split(",");
				//支店定義ファイルもしくは商品定義ファイルのフォーマットが不正
				if((read.length != 2) || (!read[0].matches(regular))) {
					System.out.println(FILE_INVALID_FORMAT);
					return false;
				}
				//コードとそれに対応する名前を保持
				names.put(read[0], read[1]);
				//合計金額の初期化
				sales.put(read[0], (long) 0);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		
		BufferedWriter bw = null;
		try {
			File newFile = new File(path, fileName);
			//ファイル作成
			newFile.createNewFile();
			FileWriter fw = new FileWriter(newFile);
			bw = new BufferedWriter(fw);
			for (String key : names.keySet()) { 
				//ファイルに書き込み
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				//改行
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
